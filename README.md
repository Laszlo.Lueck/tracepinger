# TracePinger



## Start the Container with (self signed) Certificate
- the port 7247 is the https port
- the port 5261 is the http port (you don´t need them).

There is no need to put a cert-path / password from outside, because, it is a core component of the Dockerfile.
Unless you will use an external certificate (copy from outside)

```shell
docker run --name tracepinger -p 0.0.0.0:5261:5261 -p 0.0.0.0:7247:7247 laszlo/tracepinger
```

## Requesting the container
### Ping
The easiest way to request the container is per curl.
Here is a simple example (for secure request via https)
```shell
curl --insecure -X 'GET' \
  'https://localhost:7247/api/ping?ipOrHostName=one.one.one.one&ttl=128&timeout=1000&resolve=true' \
  -H 'accept: */*'
```

or (for insecure request via http)

```shell
curl -X 'GET' \
  'http://localhost:5261/api/ping?ipOrHostName=one.one.one.one&ttl=128&timeout=1000&resolve=true' \
  -H 'accept: */*'
```
The result should be a response like this:
```shell
{"hotstName":"one.one.one.one","ipAddress":"1.1.1.1","addressFamily":"IpV4","responseTimeMs":4}
```