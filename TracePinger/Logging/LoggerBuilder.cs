﻿using System.Runtime.CompilerServices;
using ILogger = Serilog.ILogger;

namespace TracePinger.Logging;

public static class LoggerExtensions
{
    public static ILogger ExtendedContext(this ILogger logger, Type type,
        [CallerMemberName] string memberName = "",
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0) =>
        ExtendLogger(logger, memberName, sourceFilePath, sourceLineNumber, type);

    public static ILogger ExtendedContext<T>(this ILogger logger,
        [CallerMemberName] string memberName = "",
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0
    ) => ExtendLogger(logger, memberName, sourceFilePath, sourceLineNumber, typeof(T));

    private static Func<ILogger, string, string, int, Type, ILogger> ExtendLogger =
        (logger, memberName, sourceFilePath, sourceLineNumber, type) => logger
            .ForContext("MemberName", memberName)
            .ForContext("FilePath", sourceFilePath)
            .ForContext("LineNumber", sourceLineNumber)
            .ForContext("Source", type
            );
}