using System.Net;
using LanguageExt;
using LibPing.Net;

namespace TracePinger.Services;

public interface IIcmpService
{
    Task<Either<IcmpLeftResult, IcmpResponse>> GetPing(string ipOrHostName, int timeout, int ttl,
        CancellationToken token);

    Task<string> ResolveHostName(IPEndPoint endpoint, CancellationToken token);
}