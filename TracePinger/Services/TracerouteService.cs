using System.Net;
using LanguageExt;
using LibPing.Net;
using TracePinger.Utils;
using TracePinger.Logging;
using ILogger = Serilog.ILogger;

namespace TracePinger.Services;

public class TracerouteService : ITracerouteService
{
    private readonly ILogger _logger;
    private readonly IUtils _utils;
    
    public TracerouteService(ILogger logger, IUtils utils)
    {
        _logger = logger.ExtendedContext<TracerouteService>();
        _utils = utils;
    }
    
    public IAsyncEnumerable<Either<TracerouteLeftResult, TracerouteResponse>> GetTraceroute(string ipOrHostName, int timeout, int ttl, CancellationToken cancellationToken)
    {
        return  Traceroute.DoTraceroute(ipOrHostName, ttl, timeout);
    }

    public async Task<string> ResolveHostName(IPEndPoint endpoint, CancellationToken token)
    {
        return await _utils.AsyncResolveHostName(endpoint.Address.ToString(), token);
    }
}