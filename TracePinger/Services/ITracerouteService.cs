using System.Net;
using LanguageExt;
using LibPing.Net;

namespace TracePinger.Services;

public interface ITracerouteService
{
    IAsyncEnumerable<Either<TracerouteLeftResult, TracerouteResponse>> GetTraceroute(string ipOrHostName, int timeout, int ttl, CancellationToken cancellationToken);

    Task<string> ResolveHostName(IPEndPoint endpoint, CancellationToken token);
}