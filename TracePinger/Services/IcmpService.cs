using System.Net;
using LanguageExt;
using LibPing.Net;
using TracePinger.Logging;
using TracePinger.Utils;
using ILogger = Serilog.ILogger;

namespace TracePinger.Services;

public class IcmpService : IIcmpService
{
    private readonly ILogger _logger;
    private readonly IUtils _utils;

    public IcmpService(ILogger log, IUtils utils)
    {
        _logger = log.ExtendedContext<IcmpService>();
        _utils = utils;
    }

    public async Task<Either<IcmpLeftResult, IcmpResponse>> GetPing(string ipOrHostName, int timeout, int ttl,
        CancellationToken token)
    {
        return await Icmp.PingSecure(ipOrHostName, ttl, timeout, token);
    }

    public async Task<string> ResolveHostName(IPEndPoint endpoint, CancellationToken token)
    {
        return await _utils.AsyncResolveHostName(endpoint.Address.ToString(), token);
    }
}