﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace TracePinger.Endpoints.Ping;

public class PingRequest
{
    [FromQuery(Name = "ipOrHostName"), Required] public string? IpOrHostName { get; set; }
    [FromQuery(Name = "timeout"), DefaultValue(3000)] public int TimeOut { get; set; }
    [FromQuery(Name = "ttl"), DefaultValue(128)] public int Ttl { get; set; } 
    [FromQuery(Name = "resolveHostName"), DefaultValue(true)] public bool ResolveHostName { get; set; }
}