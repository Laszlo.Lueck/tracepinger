using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TracePinger.Services;

namespace TracePinger.Endpoints.Ping;

public class PingEndpoint : EndpointBaseAsync.WithRequest<PingRequest>.WithActionResult
{
    private readonly IIcmpService _icmpService;

    public PingEndpoint(IIcmpService icmpService)
    {
        _icmpService = icmpService;
    }

    [HttpGet("/api/admin/ping")]
    [SwaggerOperation(
        Summary = "call a icmp ping command from a given ip or host",
        Description = "retrieve some information about the host to ping and returns some results",
        OperationId = "0D8F8889-4553-486B-8233-8CD59252F35C",
        Tags = new[] {"Ping"}
    )]
    [ProducesResponseType(typeof(PingResponse), 200)]
    [ProducesResponseType(500)]
    public override async Task<ActionResult> HandleAsync([FromQuery] PingRequest request,
        CancellationToken cancellationToken = default)
    {
        var token = new CancellationTokenSource(4000).Token;

        return await (await _icmpService
                .GetPing(request.IpOrHostName!, request.TimeOut, request.Ttl, token))
            .MatchAsync<ActionResult>(async right =>
                {
                    var hostResult = request.ResolveHostName && right.Origin is not null
                        ? await _icmpService.ResolveHostName(right.Origin, token)
                        : request.IpOrHostName;
                    var ipAddress = right.Origin is not null ? right.Origin.Address.ToString() : "not resolvable";

                    var detail = new PingResponse()
                    {
                        AddressFamily = right.AddressFamily.ToString(),
                        HotstName = hostResult,
                        IpAddress = ipAddress,
                        ResponseTimeMs = right.RoundTripTime
                    };

                    return Ok(detail);
                },
                async left =>
                {
                    return await Task.Run(() => Problem(
                        detail: left.exception.StackTrace,
                        title: $"{left.exception.Message} :: {left.ipOrHostName}",
                        statusCode: 500
                    ), token);
                });
    }
}