﻿using System.Text.Json.Serialization;

namespace TracePinger.Endpoints.Ping;

public class PingResponse
{
    [JsonPropertyName("hostName")]public string? HotstName { get; set; }
    
    [JsonPropertyName("ipAddress")]public string? IpAddress { get; set; }
    [JsonPropertyName("addressFamily")]public string? AddressFamily { get; set; }
    [JsonPropertyName("responseTimeMs")]public long ResponseTimeMs { get; set; }
}