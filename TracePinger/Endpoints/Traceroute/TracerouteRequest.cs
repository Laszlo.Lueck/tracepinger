using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace TracePinger.Endpoints.Traceroute;

public class TracerouteRequest
{
    [FromQuery(Name = "ipOrHostName"), Required] public string? IpOrHostName { get; set; }
    [FromQuery(Name = "timeout"), DefaultValue(3000)] public int TimeOut { get; set; }
    [FromQuery(Name = "ttl"), DefaultValue(30)] public int Ttl { get; set; }
    [FromQuery(Name = "resolveHostNames"),DefaultValue(true)] public bool ResolveHostNames { get; set; } 
}