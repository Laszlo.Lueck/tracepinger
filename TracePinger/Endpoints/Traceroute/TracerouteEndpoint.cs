using System.Net;
using Ardalis.ApiEndpoints;
using LanguageExt;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TracePinger.Services;

namespace TracePinger.Endpoints.Traceroute;

public class TracerouteEndpoint : EndpointBaseAsync.WithRequest<TracerouteRequest>.WithActionResult
{
    private readonly ITracerouteService _tracerouteService;

    public TracerouteEndpoint(ITracerouteService tracerouteService)
    {
        _tracerouteService = tracerouteService;
    }

    [HttpGet("/api/admin/traceroute")]
    [SwaggerOperation(
        Summary = "calls a traceroute command",
        Description = "call a traceroute, which is a chain of ping from a given ip or host",
        OperationId = "EB8C1848-14CA-456D-96A8-663C6381E452",
        Tags = new[] {"Traceroute"}
    )]
    [ProducesResponseType(typeof(TracerouteResponse), 200)]
    [ProducesResponseType(500)]
    public override async Task<ActionResult> HandleAsync([FromQuery] TracerouteRequest request,
        CancellationToken cancellationToken = default)
    {
        var token = new CancellationTokenSource(1000).Token;
        var results = await _tracerouteService.GetTraceroute(request.IpOrHostName!, request.TimeOut, request.Ttl, token)
            .ToEnumerable()
            .Map(async either =>
            {
                return await either.MatchAsync(async right =>
                {
                    var innerToken = new CancellationTokenSource(100).Token;
                    innerToken.ThrowIfCancellationRequested();
                    
                    var hostName = request.ResolveHostNames && right.Origin is not null
                        ? await _tracerouteService.ResolveHostName(right.Origin, innerToken)
                        : right.Origin?.Address.ToString() ?? "Not available";

                    var ipAddress = right.Origin is not null
                        ? right.Origin.Address.ToString()
                        : "not resolvable";

                    return new TracerouteResponseElement
                    {
                        AddressFamily = right.AddressFamily.ToString(),
                        Hop = right.Hop,
                        HostName = hostName,
                        IpAddress = ipAddress,
                        ResponseTimeMs = right.RoundTripTime,
                        State = 0,
                        StateMessage = "ok"
                    };
                }, left => new TracerouteResponseElement
                {
                    Hop = left.Hop,
                    ResponseTimeMs = 0,
                    State = -1,
                    StateMessage = left.Exception.Message,
                    StateDetail = left.Exception.StackTrace
                });
            })
            .SequenceParallel(10);

        var response = new TracerouteResponse
        {
            Responses = results.OrderBy(d => d.Hop)
        };
        return Ok(response);
    }
}