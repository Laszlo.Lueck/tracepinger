using System.Text.Json.Serialization;

namespace TracePinger.Endpoints.Traceroute;

public class TracerouteResponse
{
    [JsonPropertyName("responses")] public IEnumerable<TracerouteResponseElement>? Responses { get; set; }
}

public class TracerouteResponseElement
{
    [JsonPropertyName("hop")] public int Hop { get; set; }
    [JsonPropertyName("hostName")] public string? HostName { get; set; }
    [JsonPropertyName("ipAddress")] public string? IpAddress { get; set; }
    [JsonPropertyName("addressFamily")] public string? AddressFamily { get; set; }
    [JsonPropertyName("responseTimeMs")] public long ResponseTimeMs { get; set; }
    [JsonPropertyName("state")] public int State { get; set; }
    [JsonPropertyName("stateMessage")] public string? StateMessage { get; set; }
    [JsonPropertyName("stateDetail")] public string? StateDetail { get; set; }
}