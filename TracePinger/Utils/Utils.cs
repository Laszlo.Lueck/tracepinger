﻿using System.Diagnostics;
using System.Net;
using DnsClient;
using TracePinger.Logging;
using ILogger = Serilog.ILogger;

namespace TracePinger.Utils;

public interface IUtils
{
    Task<string> AsyncResolveHostName(string ipAddress, CancellationToken token);
}


public class Utils : IUtils
{

    private readonly ILogger _logger;
    public Utils(ILogger logger)
    {
        _logger = logger.ExtendedContext(typeof(Utils));
    }

    public async Task<string> AsyncResolveHostName(string ipAddress, CancellationToken token)
    {
        var sw = Stopwatch.StartNew();
        
        try
        {
            var client = new LookupClient();
            if (IPAddress.TryParse(ipAddress, out var ip))
            {
                _logger.Information("Try to resolve {IpAddress}", ip.ToString());
                return await Task.Run(() =>
                {
                    var se = client.GetHostEntry(ip);
                    return se?.HostName ?? ipAddress;
                }, token);
            }

            return ipAddress;
        }
        catch (Exception exception)
        {
            _logger.Error(exception, "An error occured");
            return await Task.Run(() => ipAddress);
        }
        finally
        {
            sw.Stop();
            _logger.Information("Resolve of {IP} takes {Elapsed} ms",ipAddress, sw.ElapsedMilliseconds);
        }
    }
}