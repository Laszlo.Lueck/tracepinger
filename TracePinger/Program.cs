using Microsoft.OpenApi.Models;
using Serilog;
using Serilog.Enrichers.ShortTypeName;
using Serilog.Sinks.SystemConsole.Themes;
using TracePinger.Services;
using TracePinger.Utils;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((_, loggerConfiguration) =>
{
    loggerConfiguration
        .Enrich.WithShortTypeName()
        .Enrich.FromLogContext()
        .MinimumLevel.Information()
        .WriteTo.Console(theme: AnsiConsoleTheme.Code,
            outputTemplate:
            "[{Timestamp:yyy-MM-dd HH:mm:ss} {Level:u3} {ShortTypeName}] {Message:lj}{NewLine}{Exception}");
    
});

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddScoped<IIcmpService, IcmpService>();
builder.Services.AddScoped<ITracerouteService, TracerouteService>();
builder.Services.AddSingleton<IUtils, Utils>();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo{Title = "TracePinger", Version = "v1"});
    c.EnableAnnotations();
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger(c =>
    {
        c.SerializeAsV2 = true;
    });
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "TracePinger"));
}

app.UseHttpsRedirection();
app.MapControllers();
app.UseRouting();
app.UseAuthorization();
app.UseEndpoints(endpoints => endpoints.MapControllers());
app.UseStaticFiles();

app.Run();